from django.conf.urls import url
from rest_framework import routers
from .views import MeasuredTempView, SensorView
router = routers.DefaultRouter()
urlpatterns = router.urls
urlpatterns = [
    url(r'^measured_temp/',  MeasuredTempView.as_view(), name='measured_temp'),
    url(r'^sensors/',  SensorView.as_view(), name='sensors'),
]
