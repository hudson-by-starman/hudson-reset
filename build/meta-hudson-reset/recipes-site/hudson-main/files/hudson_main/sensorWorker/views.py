from rest_framework import viewsets
from .models import measured_temperature, sensors
from .serializers import MeasuredTempSerializer, SensorsSerializer
from rest_framework.response import Response
from rest_framework.views import APIView

class MeasuredTempView(APIView):
    def get(self, request, format=None):
        queryset = measured_temperature.objects.all()
        serializer = MeasuredTempSerializer(queryset, many=True)
        return Response(serializer.data)

class SensorView(APIView):
    def get(self, request, format=None):
        queryset = sensors.objects.all()
        serializer = SensorsSerializer(queryset, many=True)
        return Response(serializer.data)
