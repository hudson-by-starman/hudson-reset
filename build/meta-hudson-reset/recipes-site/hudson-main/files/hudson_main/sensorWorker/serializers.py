from rest_framework import serializers
from .models import measured_temperature, sensors
class MeasuredTempSerializer(serializers.ModelSerializer):
    class Meta:
        model = measured_temperature
        fields = (
            'measured_temp',
            'measured_time',
            'sensorID',
        )

class SensorsSerializer(serializers.ModelSerializer):
    class Meta:
        model = sensors
        fields = (
            'type',
            'sensorID',
            'connectedDeviceID',
        )
