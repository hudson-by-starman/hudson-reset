#!/bin/bash
set -x
########SETUP MUTUAL TLS########

commonNameCa=$(head /dev/urandom | tr -dc A-Za-z0-9 | cut -c1-64)
certPassword=$(head /dev/urandom | tr -dc A-Za-z0-9 | cut -c1-64)


local_ip=$(/sbin/ifconfig wlan0 | grep "inet\|inet6" | awk -F' ' '{print $2}' | head -n1)
external_ip=$(curl http://ipecho.net/plain; echo)

if [ -z "$local_ip" ]; then
	local_ip="0.0.0.0"
fi


#Create the external certs
openssl genrsa -aes256 -passout pass:$certPassword -out /hudson/ca/externalCa.key 4096
chmod 400 /hudson/ca/externalCa.key
openssl req -new -x509 -sha256 -passin pass:$certPassword -days 730 -key /hudson/ca/externalCa.key -out /hudson/ca/externalCa.crt \
    -subj "/C=GB/ST=HERE/L=HERE/O=Hudson/OU=Hudson/CN=$commonNameCa"
chmod 444 /hudson/ca/externalCa.crt

openssl genrsa -out /hudson/ca/externalHudson.key 2048
chmod 400 /hudson/ca/externalHudson.key

openssl req -new -key /hudson/ca/externalHudson.key -sha256 \
    -subj "/C=GB/ST=CA/O=Hudson/CN=$external_ip" \
	-config <(cat /etc/ssl/openssl.cnf \
    <(printf "\n[SAN]\nsubjectAltName=IP:$local_ip,IP:$external_ip")) \
	-out /hudson/ca/externalHudson.csr

openssl x509 -req \
    -days 730 \
    -in /hudson/ca/externalHudson.csr \
    -CA /hudson/ca/externalCa.crt \
    -CAkey /hudson/ca/externalCa.key \
    -CAcreateserial \
    -passin pass:$certPassword \
    -out /hudson/ca/externalHudson.crt

openssl genrsa -out /hudson/ca/externalClient.key 2048

openssl req -new -key /hudson/ca/externalClient.key \
    -subj "/C=GB/ST=CA/O=Hudson/CN=$external_ip" \
	-config <(cat /etc/ssl/openssl.cnf \
    <(printf "\n[SAN]\nsubjectAltName=IP:$local_ip,IP:$external_ip")) \
	-out /hudson/ca/externalClient.csr

openssl x509 -req -days 365 -sha256 \
	-in /hudson/ca/externalClient.csr \
	-CA /hudson/ca/externalCa.crt \
	-CAkey /hudson/ca/externalCa.key \
	-passin pass:$certPassword -set_serial 2 \
	-out /hudson/ca/externalClient.crt

openssl pkcs12 -export -clcerts -in /hudson/ca/externalClient.crt -inkey /hudson/ca/externalClient.key -out /hudson/ca/externalClient.p12 -password pass:hudsonPassword
chmod 444 /hudson/ca/externalClient.p12

#Copy necessary files into media folder
cp /hudson/ca/externalCa.crt /hudson/site/hudson_connect/media/externalCa.crt
cp /hudson/ca/externalClient.p12 /hudson/site/hudson_connect/media/externalClient.p12

systemctl restart hudson_main_nginx
########DONE SETUP MUTUAL TLS########
