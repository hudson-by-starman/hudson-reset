DESCRIPTION = "Fast implementation of asyncio event loop on top of libuv"
HOMEPAGE = "https://pypi.python.org/pypi/uvloop"
SECTION = "devel/python"
LICENSE = "MIT & Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE-MIT;md5=3bd0b2e751776b370b2151ac508af939 \
                    file://LICENSE-APACHE;md5=57700cee6d14d45039259183f3c50ef1"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRCNAME = "uvloop"

PYPI_PACKAGE = "uvloop"

SRC_URI[md5sum] = "43d7986ac163267ab4d61eac950d34de"
SRC_URI[sha256sum] = "c48692bf4587ce281d641087658eca275a5ad3b63c78297bbded96570ae9ce8f"

S = "${WORKDIR}/${SRCNAME}-${PV}"

DEPENDS = "${PYTHON_PN}-cython-native"

BBCLASSEXTEND = "native"

inherit setuptools3 pypi

do_compile_prepend() {
    export LIBUV_CONFIGURE_HOST=${HOST_SYS}
}

do_install_prepend() {
    export LIBUV_CONFIGURE_HOST=${HOST_SYS}
}
