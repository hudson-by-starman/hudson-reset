SUMMARY = "h11 Python Module"
LICENSE = "MIT"
DESCRIPTION = "A pure-Python, bring-your-own-I/O implementation of HTTP/1.1"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=f5501d19c3116f4aaeef89369f458693"
PYPI_PACKAGE = "h11"
inherit setuptools3 pypi
SRC_URI[md5sum] = "979d784feea6d576582558ac742005ad"
SRC_URI[sha256sum] = "acca6a44cb52a32ab442b1779adf0875c443c689e9e028f8d831a3769f9c5208"
