<i>This is an overview of the Hudson Reset software and Operating System.
For information on the Hudson system as a whole,
[see here](https://gitlab.com/hudson-by-starman/hudson-meta)</i>

# Hudson Reset

**Problem:** The Hudsons Brain Device is P2P. But to protect itself from unwanted users it has a password. Without this password, no user can access the device without physically removing the SD card and editing files on it manually, and even then, the attacker couldn't access user files if the user enabled encryption.

**But what happens if the original Admin forgets their password?**

Well, the device is bricked. Adding any method to just reset the password is a security risk.

To solve the bricking problem and make the device usable again, the device needs to be wiped. You can't expect an average consumer to be able to fully reinstall the Hudson OS, all the required packages and configure the device correctly. So I created Hudson Reset. A script that functions to automate as much of this process as possible.



# Code Explained

The entire program in its current state is one simple shell script. (A GUI program written in QT is being worked on)

The script first checks if an image of the OS has already been built. If you have built an image in the past the user will be told when it was created and if they want to use it.

The user can also choose to build or download the OS.

The build portion of the process uses [yocto](https://www.yoctoproject.org/).

This creates a fully functional Hudson Embedded Operating System.

The files related to this operating system is where most of the work happens on Hudson and are located in build/meta-hudson-reset.

The image created has all Hudson OS related files setup and ready to use. The build process is automatic and works successfully on debian based systems (Arch also used successfully with added packages)

It then runs a function that asks the user to select their SD card. They will receive a prompt like this:

	Is your SD card "/dev/sdX"?(y/n)

Choosing n will continue the loop and it will ask the user about the next device in the list. The list of devices has been filtered to only show SD cards so the list will be only one device for most users.

Choosing y will continue to format every partition in the device as fat, then wipe the partitions and transfer the downloaded files over.


A random name and password is generated for the Brain which the user is asked to write down. This is actually the SSID and password of an AP network which the device will run at the start so a Hudson-Android app can connect to it.

A post-install script will create "run_start.sh" which will run once at the first boot. This turns on the AP network, sets up files that allow the user to add a WiFi network through an API and runs the web-server needed to pair with the android app. It creates a second web-server that is protected by mutual TLS encryption.

Getting this far took quite a bit of work because bug testing involved having to wait 25 minutes for the OS to install, only to find out that I had an accidental indentation in a cat command.




# Example Output

    An image of the Hudson OS already exists.
    This image is from 2019-01-27
    Enter 1 to use this image. Enter 2 to download or build a new image: 1
    Flashing the image
    Is your SD card /dev/sde?(y/n): y
	Selected device is: /dev/sde
	Wiping Device..
	Installing New Hudson OS
	Mounting New Hudson OS
	Mount success

	Your Brains new name is: Hudson-7YT5
	Please write this down

	Your Brains new password is: OI7j3Ha7MqAA
	Please write this down(It's case sensitive)

	Nice. Take that SD out and plug it into Hudsons Brain.


![](https://gitlab.com/hudson-by-starman/hudson-meta/raw/master/media/resetScript.gif)

# Into the Future

In terms of the future of Hudson Reset, I'd like to see an actual GUI program for Linux, Windows, and Mac that keeps the standard of design of the other apps.
